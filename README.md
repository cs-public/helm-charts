# Helm charts

## Install repository

Add a chart helm repository with follow commands:

```console
helm repo add cs-public https://cs-public.gitlab.io/helm-charts/
helm repo update
```

## Installing the chart

Export default values of ``ctrlstack-agent`` chart to file ``values.yaml``:

```console
helm show values cs-public/ctrlstack-agent > values.yaml
```

Edit ``values.yaml`` file changing the values accordingly. The only required settings are the credentials used to authorize the agent.

Install the chart:

```console
helm install ctrlstack-agent cs-public/ctrlstack-agent -f values.yaml
```

